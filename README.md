# README #

Qwake is an iPhone app that makes US Geological Survey Data available for browsing. You can view view earthquakes on a map interface, set a minimum magnitude for earthquakes displayed, scrub through earthquakes that have occurred over the past month, and view details about each earthquake.

Version 0.0.0.1

### How do I get set up? ###

1. Download and install Xcode
1. Clone the qwake repository
1. Open Qwake.xcodeproj with Xcode
1. Command+R to build and run

### How is this licensed? ###

Source code is licensed under the MIT License. See the LICENSE file.

Icons are courtesy of Icons8 https://icons8.com/ and are used through a Creative Commons Attribution-NoDerivs 3.0 Unported license https://creativecommons.org/licenses/by-nd/3.0/

### Who do I talk to? ###

Will Ellis wiellis@vt.edu

### Issues ###

Functionality right now is conconcentrated a little too much MapVC. Should probably break the networking into its own class.

Right now USGS data is downloaded all at once when the app starts. This permits the high-speed temporal scrubbing feature of the app. Downloading all the data at once works ok because the USGS API only makes about the last month of data available, and the data is relatively sparse anyway. However, data should only be downloaded as needed ideally.

There seems to be a problem with the app not releasing some data as annotations (pins) are added and removed from the map.