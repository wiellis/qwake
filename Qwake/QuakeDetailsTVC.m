//
//  QuakeDetailsTVC.m
//  Qwake
//
//  Created by William Ellis on 10/5/15.
//  Copyright © 2015 Will T. Ellis. All rights reserved.
//

#import "QuakeDetailsTVC.h"

@implementation QuakeDetailsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done)];
    self.navigationItem.leftBarButtonItem = doneButton;
}

- (IBAction)done {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detailsCell" forIndexPath:indexPath];
    UILabel * keyLabel = (UILabel*)[cell viewWithTag:1];
    UILabel * valueLabel = (UILabel*)[cell viewWithTag:2];
    switch (indexPath.row) {
        case 0:
            keyLabel.text = @"Location";
            valueLabel.text = self.quake.place;
            break;
        case 1:
            keyLabel.text = @"Magnitude";
            valueLabel.text = [NSString stringWithFormat:@"%.1lf", [self.quake.magnitude doubleValue]];
            break;
        case 2:
            keyLabel.text = @"Date";
            valueLabel.text = [NSDateFormatter localizedStringFromDate:self.quake.date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
            break;
        case 3:
            keyLabel.text = @"Coordinates";
            valueLabel.text = [NSString stringWithFormat:@"%lf, %lf", self.quake.coordinate.latitude, self.quake.coordinate.longitude];
            break;
        case 4:
            keyLabel.text = @"Depth";
            valueLabel.text = [NSString stringWithFormat:@"%.2lf km", [self.quake.depth doubleValue]];
            break;
        default:
            break;
    }
    return cell;
}
@end
