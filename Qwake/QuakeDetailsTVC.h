//
//  QuakeDetailsTVC.h
//  Qwake
//
//  Created by William Ellis on 10/5/15.
//  Copyright © 2015 Will T. Ellis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuakeAnnotation.h"

@interface QuakeDetailsTVC : UITableViewController

@property (strong, nonatomic) QuakeAnnotation * quake;

@end
