//
//  QuakeAnnotation.h
//  Qwake
//
//  Created by William Ellis on 10/4/15.
//  Copyright © 2015 Will T. Ellis. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

@interface QuakeAnnotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@property (strong, nonatomic) NSNumber * magnitude;
@property (strong, nonatomic) NSString * place;
@property (strong, nonatomic) NSDate * date;
@property (strong, nonatomic) NSNumber * depth;

@end
