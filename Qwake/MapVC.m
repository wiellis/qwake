//
//  ViewController.m
//  Qwake
//
//  Created by William Ellis on 10/4/15.
//  Copyright © 2015 Will T. Ellis. All rights reserved.
//

#import "MapVC.h"
#import "QuakeAnnotation.h"
#import "QuakeDetailsTVC.h"

@import MapKit;

#define API_URI @"http://ehp2-earthquake.wr.usgs.gov/fdsnws/event/1/"
#define QUERY_METHOD @"query"
#define JSON_ACCEPT_HEADER @"application/json"
#define RESPONSE_LIMIT 3000
#define FEATURES_KEY @"features"
#define PROPERTIES_KEY @"properties"

#define SECONDS_IN_DAY (60*60*24)
#define SEC_TO_MSEC 0.001

#define TIME_WINDOW_IN_DAYS 1
#define FULL_TIME_SPAN_IN_DAYS 31
#define MIN_MAGNITUDE 3.0
#define MAX_MAGNITUDE 8.0

@interface MapVC ()

@property (weak, nonatomic) IBOutlet MKMapView *mapVC;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel * label;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (strong, nonatomic) NSURLSession * session;
@property (strong, nonatomic) NSMutableArray<id<MKAnnotation>> * quakes;
@property (strong, nonatomic) NSMutableSet<NSString*> * quakeIDs;
@property (strong, nonatomic) NSDate * endDate;
@property (strong, nonatomic, readonly) NSDate * startDate;
@property (nonatomic) float minMagnitude;
@property (nonatomic) float datesSliderValue;
@property (nonatomic) float magSliderValue;

@end

@implementation MapVC

- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        self.slider.value = self.datesSliderValue;
        [self.slider setMinimumValueImage:[UIImage imageNamed:@"Minus1Month"]];
        [self.slider setMaximumValueImage:[UIImage imageNamed:@"Minus1Day"]];
    } else {
        [self.slider setMinimumValueImage:[UIImage imageNamed:@"Mag3"]];
        [self.slider setMaximumValueImage:[UIImage imageNamed:@"Mag8"]];
        self.slider.value = self.magSliderValue;
    }
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.datesSliderValue = sender.value;
    } else {
        self.magSliderValue = sender.value;
    }
    [self updateVisibleQuakes];
}

- (void)updateVisibleQuakes {
    double timeInterval = (FULL_TIME_SPAN_IN_DAYS - TIME_WINDOW_IN_DAYS) * (self.datesSliderValue - self.slider.maximumValue) * SECONDS_IN_DAY;
    self.endDate = [NSDate dateWithTimeInterval:timeInterval sinceDate:[NSDate date]];
    self.minMagnitude = (MAX_MAGNITUDE - MIN_MAGNITUDE) * self.magSliderValue + MIN_MAGNITUDE;
    
    // 1. Get the new set of filtered quakes
    NSMutableSet * newQuakes = [NSMutableSet setWithArray:[self.quakes objectsAtIndexes:[self.quakes indexesOfObjectsWithOptions:NSEnumerationConcurrent passingTest:^BOOL(QuakeAnnotation*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [self.startDate compare:obj.date] != NSOrderedDescending && [self.endDate compare:obj.date] != NSOrderedAscending && [obj.magnitude floatValue] >= self.minMagnitude;
    }]]];
    NSSet * quakes = [NSMutableSet setWithArray:[self.mapVC annotations]];
    
    // 2. Get the set of now-filtered-out quakes
    NSMutableSet * staleQuakes = [NSMutableSet setWithSet:quakes];
    [staleQuakes minusSet:newQuakes];

    // 3. Remove the now-filtered-out quakes from the map, add the now-filtered in quakes to the map
    [newQuakes minusSet:quakes];
    [self.mapVC removeAnnotations:[staleQuakes allObjects]];
    [self.mapVC addAnnotations:[newQuakes allObjects]];
    [self updateLabel];
}

- (NSURLSession*)session {
    if (!_session) {
        NSURLSessionConfiguration * sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        [sessionConfig setHTTPAdditionalHeaders:@{@"Accept": JSON_ACCEPT_HEADER}];
        _session = [NSURLSession sessionWithConfiguration:sessionConfig];
    }
    return _session;
}

- (NSMutableArray*)quakes {
    return _quakes ? _quakes : (_quakes = [NSMutableArray array]);
}

- (NSDate*)startDate {
    return [NSDate dateWithTimeInterval:-TIME_WINDOW_IN_DAYS*SECONDS_IN_DAY sinceDate:self.endDate];
}

- (NSDate*)endDate {
    return _endDate ? _endDate : (_endDate = [NSDate date]);
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _minMagnitude = MIN_MAGNITUDE;
        _datesSliderValue = 1.0f;
        _magSliderValue = 0.0f;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.mapVC.delegate = self;
    [self fetchQuakes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchQuakes
{
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    NSDate * endDate = [NSDate date];
    NSDate * startDate = [NSDate dateWithTimeInterval:-1*FULL_TIME_SPAN_IN_DAYS*SECONDS_IN_DAY sinceDate:endDate];
    NSString * queryString = [self buildQueryStringWithStartDate:startDate endDate:endDate minMagnitude:self.minMagnitude];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", API_URI, queryString]];
    [[self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *connectionError) {
        if (data.length > 0 && !connectionError) {
            NSError * jsonError = nil;
            NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (!jsonError) {
                    [self processQuakeFeatures:[responseDict objectForKey:FEATURES_KEY]];
                }
                [self.activityIndicator stopAnimating];
                self.activityIndicator.hidden = YES;
                [self updateLabel];
            });
        }
    }] resume];
}

- (NSString*)buildQueryStringWithStartDate:(NSDate*)startDate endDate:(NSDate*)endDate minMagnitude:(float)minMagnitude {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    return [NSString stringWithFormat:@"%@?starttime=%@&endtime=%@&minmagnitude=%f&limit=%i&format=%@", QUERY_METHOD, [dateFormatter stringFromDate:startDate], [dateFormatter stringFromDate:endDate], minMagnitude, RESPONSE_LIMIT, @"geojson"];
}

- (void)processQuakeFeatures:(NSArray*)features {
    for (NSDictionary *featureDict in features) {
        NSNumber * mag = [[featureDict objectForKey:PROPERTIES_KEY] objectForKey:@"mag"];
        NSString * place = [[featureDict objectForKey:PROPERTIES_KEY] objectForKey:@"place"];
        NSNumber * time = [[featureDict objectForKey:PROPERTIES_KEY] objectForKey:@"time"];
        NSString * quakeID = [featureDict objectForKey:@"id"];
        NSArray * coords = [[featureDict objectForKey:@"geometry"] objectForKey:@"coordinates"];
        if (mag && place && time && quakeID && [coords count] == 3) {
            if ([self.quakeIDs member:quakeID]) {
                continue;
            }
            CLLocationDegrees lon = [((NSNumber*)[coords objectAtIndex:0]) doubleValue];
            CLLocationDegrees lat = [((NSNumber*)[coords objectAtIndex:1]) doubleValue];
            QuakeAnnotation * quake = [[QuakeAnnotation alloc] init];
            quake.coordinate = CLLocationCoordinate2DMake(lat, lon);
            quake.title = [NSString stringWithFormat:@"M%@ %@", [mag stringValue], place];
            quake.date = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]*SEC_TO_MSEC];
            quake.magnitude = mag;
            quake.place = place;
            quake.depth = (NSNumber*)[coords objectAtIndex:2];;
            quake.subtitle = [NSDateFormatter localizedStringFromDate:quake.date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];

            NSUInteger newIndex = [self.quakes indexOfObject:quake inSortedRange:(NSRange){0, [self.quakes count]} options:NSBinarySearchingInsertionIndex usingComparator:^(QuakeAnnotation* obj1, QuakeAnnotation* obj2){
                return [obj1.date compare:obj2.date];
            }];
            
            [self.quakes insertObject:quake atIndex:newIndex];
            [self.quakeIDs addObject:quakeID];
            if ([self.startDate compare:quake.date] != NSOrderedDescending && [self.endDate compare:quake.date] != NSOrderedAscending) {
                [self.mapVC addAnnotation:quake];
            }
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue destinationViewController] isKindOfClass:[UINavigationController class]])
    {
        UINavigationController * destinationVC = (UINavigationController*)[segue destinationViewController];
        if ([destinationVC.topViewController isKindOfClass:[QuakeDetailsTVC class]]) {
            QuakeDetailsTVC * quakeDetailsTVC = (QuakeDetailsTVC*)destinationVC.topViewController;
            quakeDetailsTVC.quake = [sender respondsToSelector:@selector(annotation)] ? [sender annotation] : nil;
        }
    }
}

- (void)updateLabel {
    self.label.text = [NSString stringWithFormat:@"Earthquakes between %@ and %@ (M > %.1f)", [NSDateFormatter localizedStringFromDate:self.startDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle], [NSDateFormatter localizedStringFromDate:self.endDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle], self.minMagnitude];
    self.label.hidden = NO;
}
#pragma mark - MKMapViewDelegate

- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"defaultPin"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.canShowCallout = YES;
    annotationView.annotation = annotation;
    return annotationView;
}

-(void) mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    [self performSegueWithIdentifier:@"earthquakeDetailsSegue" sender:view];
}
@end
