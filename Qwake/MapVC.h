//
//  ViewController.h
//  Qwake
//
//  Created by William Ellis on 10/4/15.
//  Copyright © 2015 Will T. Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;

@interface MapVC : UIViewController <MKMapViewDelegate>


@end

